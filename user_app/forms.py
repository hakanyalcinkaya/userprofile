from django.forms import ModelForm
from django import forms
from .models import userProfile
from django.core.exceptions import ValidationError




class UserProfileForm(ModelForm):
    class Meta:
        model = userProfile
        fields = [
            'first_name',
            'last_name',
            'email',
            'gender',

        ]


    def clean_first_name(self, *args, **kwargs):
        first_name = self.cleaned_data.get('first_name')
        if len(first_name) < 2:
            raise forms.ValidationError("Lutfen gecerli isim giriniz")
        return first_name

    def clean_last_name(self, *args, **kwargs):
        last_name = self.cleaned_data.get('last_name')
        if len(last_name) < 2:
            raise forms.ValidationError("Lutfen gecerli isim giriniz")
        return last_name


