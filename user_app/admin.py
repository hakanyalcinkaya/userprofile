from django.contrib import admin
from .models import userProfile

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'gender', 'slug', 'slug2']
    list_display_links = ('first_name', 'slug')
    search_fields = ['slug']




admin.site.register(userProfile, UserProfileAdmin)
